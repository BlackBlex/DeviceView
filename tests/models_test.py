import unittest

from www import utils
from www.models import Area


class ModelsTest(unittest.TestCase):
    def setUp(self) -> None:
        pass

    def test_model_as_dict(self) -> None:
        area: Area = Area(name="Akal-C")

        self.assertEqual(
            utils.as_dict(area), {"active": None, "created": None, "id": None, "modified": None, "name": "Akal-C"}
        )

    def tearDown(self) -> None:
        pass
