cryptography==38.0.1
Flask==2.2.2
SQLAlchemy==1.4.41
PyMySQL==1.0.2
python-dotenv==0.21.0
pysnmp==4.4.12
