import time
from collections.abc import Callable
from functools import wraps
from os import getenv
from typing import ParamSpec, Union

from dotenv import load_dotenv
from flask import flash, jsonify, redirect, request, session, url_for
from werkzeug.wrappers.response import Response

load_dotenv()

TIMEOUT_SESSION = int(getenv("FLASK_PERMANENT_SESSION_LIFETIME", "300"))

P = ParamSpec("P")


def login_required(function: Callable[P, Union[Response, str]]) -> Callable[P, Union[Response, str]]:
    @wraps(function)
    def decorated_function(*args: P.args, **kwargs: P.kwargs) -> Union[Response, str]:
        if "api" in request.url:
            error: dict[str, str] = {"error": "Debes estar conectado para usar la API"}
            return jsonify(error)

        if "user" not in session:
            flash("Debes estar conectado para ver esta sección", "error")

        if "user" in session and "time" in session:
            now: int = int(time.time())
            last: int = session["time"]
            delta: int = now - last

            if delta > TIMEOUT_SESSION:
                session.clear()
                flash(f"Tu sesion ha expirado despues de estar {int(TIMEOUT_SESSION/60)} minutos inactivo", "error")
            else:
                session["time"] = now

        if "_flashes" in session:
            return redirect(url_for("login_get", next=request.url))

        return function(*args, **kwargs)

    return decorated_function
