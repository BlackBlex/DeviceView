from pysnmp.hlapi import (  # type: ignore
    CommunityData,
    ContextData,
    ObjectIdentity,
    ObjectType,
    SnmpEngine,
    UdpTransportTarget,
    getCmd,
)
from snmp_info import MIBS


def __get_snmp_data(
    snmp_community: str, server: str, mib_name: str = "", oid: str = "", data: int = 0
) -> tuple[bool, str | None, str | None]:
    if mib_name and oid:
        result = getCmd(
            SnmpEngine(),
            CommunityData(snmp_community),
            UdpTransportTarget((server, 161)),
            ContextData(),
            ObjectType(ObjectIdentity(mib_name, oid, data)),
        )
    else:
        result = getCmd(
            SnmpEngine(),
            CommunityData(snmp_community),
            UdpTransportTarget((server, 161)),
            ContextData(),
            ObjectType(ObjectIdentity(oid)),
        )

    # this is what you get from SNMP agent
    error_indication, error_status, error_index, var_binds = next(result)  # type: ignore

    print(error_indication)  # type: ignore
    print(error_status)  # type: ignore
    print(error_index)  # type: ignore

    if not error_indication and not error_status:
        # each element in this list matches a sequence of `ObjectType`
        # in your request.
        # In the code above you requested just a single `ObjectType`,
        # thus we are taking just the first element from response
        oid, value = var_binds[0]
        return True, str(oid), str(value)  # type: ignore
    else:
        return False, str(error_indication), error_status  # type: ignore


def get_snmp_description(snmp_community: str, server: str) -> tuple[bool, str | None, str | None]:
    return __get_snmp_data(snmp_community, server, MIBS.System.MIB, MIBS.System.SYSTEM_DESCRIPTION)


def get_snmp_name(snmp_community: str, server: str) -> tuple[bool, str | None, str | None]:
    return __get_snmp_data(snmp_community, server, MIBS.System.MIB, MIBS.System.SYSTEM_NAME)


def get_snmp_object_id(snmp_community: str, server: str) -> tuple[bool, str | None, str | None]:
    return __get_snmp_data(snmp_community, server, MIBS.System.MIB, MIBS.System.SYSTEM_OBJECT_ID)


def get_snmp_up_time(snmp_community: str, server: str) -> tuple[bool, str | None, str | None]:
    return __get_snmp_data(snmp_community, server, MIBS.System.MIB, MIBS.System.SYSTEM_UP_TIME)


def get_snmp_contact(snmp_community: str, server: str) -> tuple[bool, str | None, str | None]:
    return __get_snmp_data(snmp_community, server, MIBS.System.MIB, MIBS.System.SYSTEM_CONTACT)


def get_snmp_location(snmp_community: str, server: str) -> tuple[bool, str | None, str | None]:
    return __get_snmp_data(snmp_community, server, MIBS.System.MIB, MIBS.System.SYSTEM_LOCATION)


def get_snmp_services(snmp_community: str, server: str) -> tuple[bool, str | None, str | None]:
    return __get_snmp_data(snmp_community, server, MIBS.System.MIB, MIBS.System.SYSTEM_SERVICES)
