import ipaddress
import json
import logging
import time
from http import HTTPStatus
from ipaddress import IPv4Network
from os import environ, getenv
from typing import Any, Union

from flask import (
    Flask,
    flash,
    jsonify,
    redirect,
    render_template,
    request,
    session,
    url_for,
)
from sqlalchemy.exc import IntegrityError
from werkzeug.security import check_password_hash as check_passwd
from werkzeug.security import generate_password_hash as generate_passwd
from werkzeug.wrappers.response import Response

from www import models, snmp, utils
from www.database import BaseModel, SessionLocal, engine
from www.decorators import login_required

environ["TZ"] = "America/Mexico_City"
time.tzset()

logging.basicConfig(format="%(asctime)s - %(levelname)s > %(name)s : %(message)s", level=logging.DEBUG)
logger = logging.getLogger(__name__)

# Creamos el objeto Flask
app = Flask(__name__)
# jsglue = JSGlue(app)

# Establecemos un poco de configuración para Flask
app.config["SESSION_COOKIE_NAME"] = getenv("FLASK_SESSION_COOKIE_NAME")
app.config["SECRET_KEY"] = getenv("FLASK_SECRET_KEY")
# app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(seconds=int(getenv('FLASK_PERMANENT_SESSION_LIFETIME')))
app.config["SQLALCHEMY_DATABASE_URI"] = getenv("SQLALCHEMY_DATABASE_URI")
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

# Establecemos algunas variables
# paths_for_guest = ['/login', '/logout', '/register', '/static']


# Iniciamos la base de datos
db = SessionLocal()

# BaseModel.metadata.drop_all(bind=engine)  # type: ignore
BaseModel.metadata.create_all(bind=engine)  # type: ignore

# Comprobamos que ya tengamos los datos por defecto creados
if db.query(models.Area).filter(models.Area.id == 1).first() is None:
    logger.info("Creating default data")
    no_area = models.Area(name="Sin área")
    admin_type = models.UserType(name="Administrator")
    user_type = models.UserType(name="User")
    no_credential = models.SNMPCredentials(version=models.SNMPVersion.V1, data='{"name": "Sin SNMP", "info": {}}')
    default_user = models.User(
        username="admin", mail="admin@localhost", passwd=generate_passwd("123"), area_id=1, user_type_id=1
    )

    db.add(no_area)
    db.add(admin_type)
    db.add(user_type)
    db.add(no_credential)
    db.add(default_user)
    db.commit()


@app.context_processor
def inject_debug() -> dict[Any, Any]:
    return dict(debug=app.debug)


# region Login system


@app.route("/login", methods=["GET"])
def login_get() -> str:
    return render_template("pages/access/login.html")


@app.route("/login", methods=["POST"])
def login_post() -> Response:
    json_data: dict[str, str] | None = request.json
    error: dict[str, str] = {}
    data: dict[str, str] = {}

    session.clear()

    if isinstance(json_data, dict):
        data = json_data

    if "mail" not in data:
        error["missing_mail"] = "Falta correo electronico"

    if "passwd" not in data:
        error["missing_passwd"] = "Falta contraseña"

    if error:
        return jsonify({"error": error})

    user: models.User | None = db.query(models.User).filter(models.User.mail == data["mail"]).first()

    if not user is None:
        if check_passwd(str(user.passwd), data["passwd"]):
            session["user"] = user.username
            session["mail"] = user.mail
            session["type"] = user.user_type_id
            session["time"] = int(time.time())

    if "user" not in session:
        error["missing_user_passwd"] = "Usuario y/o contraseña son incorrectos"

    return jsonify(error if error else {"success": "user/passwd correct"})


@app.route("/logout", methods=["GET"])
def logout() -> Response:
    if "user" in session:
        session.clear()

        flash("Se ha cerrado la sesión correctamente", "success")

    return redirect("/login")


# endregion


# region API


@app.route("/api/v1/ip/<string:ip_address>/get_network/<int:cidr>", methods=["GET"])
def api_get_network_cidr(ip_address: str, cidr: int) -> tuple[Response, int]:
    network: Union[IPv4Network, None] = utils.get_ip_range(address=ip_address, cidr=cidr)

    if isinstance(network, IPv4Network):
        return (
            jsonify(
                {
                    "network": {
                        "ip": str(network.network_address),
                        "mask_address": str(network.netmask),
                        "cidr": network.prefixlen,
                    }
                }
            ),
            HTTPStatus.OK.value,
        )
    else:
        return (
            jsonify({"network": {"ip": "not found", "mask": "not found", "cidr": "not found"}}),
            HTTPStatus.NOT_FOUND.value,
        )


@app.route("/api/v1/ip/<string:ip_address>/get_network/<string:mask>", methods=["GET"])
def api_get_network_mask(ip_address: str, mask: str) -> tuple[Response, int]:
    network: Union[IPv4Network, None] = utils.get_ip_range(address=ip_address, mask_address=mask)

    if isinstance(network, IPv4Network):
        return (
            jsonify(
                {
                    "network": {
                        "ip": str(network.network_address),
                        "mask_address": str(network.netmask),
                        "cidr": network.prefixlen,
                    }
                }
            ),
            HTTPStatus.OK.value,
        )
    else:
        return (
            jsonify({"network": {"ip": "not found", "mask": "not found", "cidr": "not found"}}),
            HTTPStatus.NOT_FOUND.value,
        )


@app.route("/api/v1/ip/<string:ip_address>/get_hosts/<int:cidr>", methods=["GET"])
def api_get_hosts_cidr(ip_address: str, cidr: int) -> tuple[Response, int]:
    network: Union[IPv4Network, None] = utils.get_ip_range(address=ip_address, cidr=cidr)

    if isinstance(network, IPv4Network):
        return jsonify({"hosts": [str(host) for host in network]}), HTTPStatus.OK.value
    else:
        return jsonify({"hosts": ["not found"]}), HTTPStatus.NOT_FOUND.value


@app.route("/api/v1/ip/<string:ip_address>/get_hosts/<string:mask>", methods=["GET"])
def api_get_hosts_mask(ip_address: str, mask: str) -> tuple[Response, int]:
    network: Union[IPv4Network, None] = utils.get_ip_range(address=ip_address, mask_address=mask)

    if isinstance(network, IPv4Network):
        return jsonify({"hosts": [str(host) for host in network]}), HTTPStatus.OK.value
    else:
        return jsonify({"hosts": ["not found"]}), HTTPStatus.NOT_FOUND.value


@app.route("/api/v1/area/add", methods=["POST"])
def api_area_add() -> tuple[Response, int]:
    json_data: dict[str, str] | None = request.json
    error: dict[str, str] = {}
    data: dict[str, str] = {}

    if isinstance(json_data, dict):
        data = json_data

    if "name" not in data:
        error["missing_name"] = "Falta nombre del area"

    if error:
        return jsonify(error), HTTPStatus.BAD_REQUEST.value

    new_area: models.Area = models.Area(name=data["name"])

    with app.app_context():
        db.add(new_area)

        error_str: str = ""

        try:
            db.commit()
        except IntegrityError as ex:
            if "Duplicate entry" in ex.orig.args[1]:
                error_str = "Ya existe un area con el mismo nombre"
            db.rollback()

        if new_area.id is not None and new_area.id > 0:
            return jsonify(utils.as_dict(new_area)), HTTPStatus.CREATED.value
        else:
            return jsonify({"error": f"{error_str}"}), HTTPStatus.UNPROCESSABLE_ENTITY.value


@app.route("/api/v1/area/delete/<int:id_area>", methods=["DELETE"])
def api_area_delete(id_area: int) -> tuple[Response, int]:
    area: models.Area | None

    with app.app_context():
        area = db.query(models.Area).filter(models.Area.id == id_area).first()

    if not id_area or area is None:
        return jsonify({"error": "Area not found"}), HTTPStatus.NOT_FOUND.value
    else:
        with app.app_context():
            db.delete(area)
            error_str: str = ""

            try:
                db.commit()
            except IntegrityError as err:
                error_str = err.orig.args[1]
                db.rollback()

        if error_str:
            return jsonify({"error": f"{error_str}"}), HTTPStatus.UNPROCESSABLE_ENTITY.value
        else:
            return jsonify({"success": f"Area [{id_area}] was successfully deleted"}), HTTPStatus.OK.value


@app.route("/api/v1/area/update", methods=["PUT"])
def api_area_update() -> tuple[Response, int]:
    json_data: dict[str, str] | None = request.json
    error: dict[str, str] = {}
    data: dict[str, str] = {}

    if isinstance(json_data, dict):
        data = json_data

    if "id" not in data:
        error["missing_id"] = "Falta id del área"

    if "name" not in data:
        error["missing_name"] = "Falta el nombre del área"

    if error:
        return jsonify(error), HTTPStatus.BAD_REQUEST.value

    area: models.Area | None

    with app.app_context():
        area = db.query(models.Area).filter(models.Area.id == int(data["id"])).first()

    if area is None:
        return jsonify({"error": "Area not found"}), HTTPStatus.NOT_FOUND.value

    setattr(area, "name", data["name"])

    with app.app_context():
        db.merge(area)
        error_str: str = ""

        try:
            db.commit()
        except IntegrityError as ex:
            if "Duplicate entry" in ex.orig.args[1]:
                error_str = "Ya existe un area con el mismo nombre"
            db.rollback()

        if area.id is not None and area.id > 0:
            return jsonify(utils.as_dict(area)), HTTPStatus.OK.value
        else:
            return jsonify({"error": f"{error_str}"}), HTTPStatus.UNPROCESSABLE_ENTITY.value


@app.route("/api/v1/device/add", methods=["POST"])
def api_device_add() -> tuple[Response, int]:
    json_data: dict[str, str] | None = request.json
    error: dict[str, str] = {}
    data: dict[str, str] = {}

    if isinstance(json_data, dict):
        data = json_data

    if "name" not in data:
        error["missing_name"] = "Falta nombre del equipo"

    if "ip" not in data:
        error["missing_ip_address"] = "Falta direccion ip del equipo"

    if "mask" not in data and "cidr" not in data:
        error["missing_mask_cidr"] = "Falta mask_address o cidr del equipo"

    if "snmp_community" not in data:
        error["missing_snmp_community"] = "Falta la comunidad snmp del equipo"

    if "area" not in data:
        error["missing_area"] = "Falta area"

    if error:
        return jsonify({"error": error}), HTTPStatus.BAD_REQUEST.value

    new_device: models.Device = models.Device(
        name=data["name"],
        ip_address=int(ipaddress.IPv4Address(data["ip"])),
        snmp_credential_id=int(data["snmp_community"]),
        area_id=int(data["area"]),
    )

    if "mask_address" in data:
        setattr(new_device, "mask_address", int(ipaddress.IPv4Address(data["mask_address"])))
    elif "cidr" in data:
        network = utils.get_ip_range(address=data["ip"], cidr=int(data["cidr"]))

        if isinstance(network, IPv4Network):
            setattr(new_device, "mask_address", int(ipaddress.IPv4Address(network.netmask)))

    with app.app_context():
        db.add(new_device)

        error_str: str = ""

        try:
            db.commit()
        except IntegrityError as err:
            if "Duplicate entry" in err.orig.args[1]:
                error_str = "Ya existe un equipo con los mismos datos"
            db.rollback()

        if new_device.id is not None and new_device.id > 0:
            return jsonify(utils.as_dict(new_device)), HTTPStatus.CREATED.value
        else:
            return jsonify({"error": f"{error_str}"}), HTTPStatus.UNPROCESSABLE_ENTITY.value


@app.route("/api/v1/device/delete/<int:id_device>", methods=["DELETE"])
def api_device_delete(id_device: int) -> tuple[Response, int]:
    device: models.Device | None

    with app.app_context():
        device = db.query(models.Device).filter(models.Device.id == id_device).first()

    if not id_device or device is None:
        return jsonify({"error": "device not found"}), HTTPStatus.NOT_FOUND.value
    else:
        with app.app_context():
            db.delete(device)
            error_str: str = ""

            try:
                db.commit()
            except IntegrityError as err:
                error_str = err.orig.args[1]
                db.rollback()

        if error_str:
            return jsonify({"error": f"{error_str}"}), HTTPStatus.UNPROCESSABLE_ENTITY.value
        else:
            return jsonify({"success": f"Device [{id_device}] was successfully deleted"}), HTTPStatus.OK.value


@app.route("/api/v1/device/update", methods=["PUT"])
def api_device_update() -> tuple[Response, int]:
    json_data: dict[str, str] | None = request.json
    error: dict[str, str] = {}
    data: dict[str, str] = {}

    if isinstance(json_data, dict):
        data = json_data

    if "id" not in data:
        error["missing_id"] = "Falta id del equipo"

    if "name" not in data:
        error["missing_name"] = "Falta nombre del equipo"

    if "ip" not in data:
        error["missing_ip_address"] = "Falta direccion ip del equipo"

    if "mask" not in data and "cidr" not in data:
        error["missing_mask_cidr"] = "Falta mask_address o cidr del equipo"

    if "snmp_community" not in data:
        error["missing_snmp_community"] = "Falta la comunidad snmp del equipo"

    if "area" not in data:
        error["missing_area"] = "Falta area"

    if error:
        return jsonify({"error": error}), HTTPStatus.BAD_REQUEST.value

    device: models.Device | None

    with app.app_context():
        device = db.query(models.Device).filter(models.Device.id == int(data["id"])).first()

    if device is None:
        return jsonify({"error": "Device not found"}), HTTPStatus.NOT_FOUND.value

    setattr(device, "name", data["name"])
    setattr(device, "ip_address", int(ipaddress.IPv4Address(data["ip"])))
    setattr(device, "snmp_credential_id", data["snmp_community"])
    setattr(device, "area_id", data["area"])

    if "mask_address" in data:
        setattr(device, "mask_address", data["mask_address"])
    elif "cidr" in data:
        network = utils.get_ip_range(address=data["ip"], cidr=int(data["cidr"]))

        if isinstance(network, IPv4Network):
            setattr(device, "mask_address", int(ipaddress.IPv4Address(network.netmask)))

    with app.app_context():
        db.merge(device)
        error_str: str = ""

        try:
            db.commit()
        except IntegrityError as err:
            if "Duplicate entry" in err.orig.args[1]:
                error_str = "Ya existe un equipo con los mismos datos"
            db.rollback()

        if device.id is not None and device.id > 0:
            return jsonify(utils.as_dict(device)), HTTPStatus.OK.value
        else:
            return jsonify({"error": f"{error_str}"}), HTTPStatus.UNPROCESSABLE_ENTITY.value


@app.route("/api/v1/snmp_community/add", methods=["POST"])
def api_snmp_community_add() -> tuple[Response, int]:
    json_data: dict[str, Any] | None = request.json
    error: dict[str, str] = {}
    data: dict[str, Any] = {}

    if isinstance(json_data, dict):
        data = json_data

    if "snmp" not in data:
        error["missing_snmp"] = "Falta información de la comunidad"
    elif not isinstance(data["snmp"], dict):
        error["missing_snmp_type"] = "La información de la comunidad deber ser un diccionario"

    if "version" not in data:
        error["missing_version"] = "Falta la versión de la comunidad"

    if error:
        return jsonify(error), HTTPStatus.BAD_REQUEST.value

    int_version: int = int(data["version"])

    new_snmp_community: models.SNMPCredentials

    if utils.check_enum_value_exist(int_version, [member.value for member in models.SNMPVersion]):
        new_snmp_community = models.SNMPCredentials(version=models.SNMPVersion(int_version))

        snmp_data: dict[str, Any] = data["snmp"]
        name_of_community: str = next(iter(snmp_data))

        match new_snmp_community.version:
            case models.SNMPVersion.V1 | models.SNMPVersion.V2:
                if "community" not in snmp_data[name_of_community]:
                    error["missing_community"] = "Falta la comunidad para la versión 1/2"
            case models.SNMPVersion.V3:
                if "user" not in snmp_data[name_of_community]:
                    error["missing_user"] = "Falta el usuario para la versión 3"

        if error:
            return jsonify(error), HTTPStatus.BAD_REQUEST.value

        setattr(new_snmp_community, "data", json.dumps({"name": name_of_community, "info": snmp_data[name_of_community]}))

        with app.app_context():
            db.add(new_snmp_community)

            error_str: str = ""

            try:
                db.commit()
            except IntegrityError as err:
                if "Duplicate entry" in err.orig.args[1]:
                    error_str = "Ya existe una comunidad con los mismos datos"
                db.rollback()

            if new_snmp_community.id is not None and new_snmp_community.id > 0:
                return jsonify(utils.as_dict(new_snmp_community)), HTTPStatus.CREATED.value
            else:
                return jsonify({"error": f"{error_str}"}), HTTPStatus.UNPROCESSABLE_ENTITY.value
    else:
        return jsonify({"error": "Version de SNMP desconocida"}), HTTPStatus.UNPROCESSABLE_ENTITY.value


@app.route("/api/v1/snmp_community/delete/<int:id_community>", methods=["DELETE"])
def api_snmp_community_delete(id_community: int) -> tuple[Response, int]:
    snmp_community: models.SNMPCredentials | None

    with app.app_context():
        snmp_community = db.query(models.SNMPCredentials).filter(models.SNMPCredentials.id == id_community).first()

    if not id_community or snmp_community is None:
        return jsonify({"error": "Community not found"}), HTTPStatus.NOT_FOUND.value
    else:
        with app.app_context():
            db.delete(snmp_community)
            error_str: str = ""

            try:
                db.commit()
            except IntegrityError as err:
                error_str = err.orig.args[1]
                db.rollback()

        if error_str:
            return jsonify({"error": f"{error_str}"}), HTTPStatus.UNPROCESSABLE_ENTITY.value
        else:
            return jsonify({"success": f"Community [{id_community}] was successfully deleted"}), HTTPStatus.OK.value


@app.route("/api/v1/snmp_community/update", methods=["PUT"])
def api_snmp_community_update() -> tuple[Response, int]:
    json_data: dict[str, Any] | None = request.json
    error: dict[str, str] = {}
    data: dict[str, Any] = {}

    if isinstance(json_data, dict):
        data = json_data

    if "id" not in data:
        error["missing_id"] = "Falta id de la comunidad"

    if "snmp" not in data:
        error["missing_snmp"] = "Falta información de la comunidad"
    elif not isinstance(data["snmp"], dict):
        error["missing_snmp_type"] = "La información de la comunidad deber ser un diccionario"

    if "version" not in data:
        error["missing_version"] = "Falta la versión de la comunidad"

    if error:
        return jsonify(error), HTTPStatus.BAD_REQUEST.value

    snmp_community: models.SNMPCredentials | None

    with app.app_context():
        snmp_community = db.query(models.SNMPCredentials).filter(models.SNMPCredentials.id == int(data["id"])).first()

    if snmp_community is None:
        return jsonify({"error": "Community not found"}), HTTPStatus.NOT_FOUND.value

    snmp_data: dict[str, Any] = data["snmp"]
    name_of_community: str = next(iter(snmp_data))

    match snmp_community.version:
        case models.SNMPVersion.V1 | models.SNMPVersion.V2:
            if "community" not in snmp_data[name_of_community]:
                error["missing_community"] = "Falta la comunidad para la versión 1/2"
        case models.SNMPVersion.V3:
            if "user" not in snmp_data[name_of_community]:
                error["missing_user"] = "Falta el usuario para la versión 3"

    if error:
        return jsonify(error), HTTPStatus.BAD_REQUEST.value

    setattr(snmp_community, "data", json.dumps({"name": name_of_community, "info": snmp_data[name_of_community]}))

    with app.app_context():
        db.merge(snmp_community)
        error_str: str = ""

        try:
            db.commit()
        except IntegrityError as err:
            if "Duplicate entry" in err.orig.args[1]:
                error_str = "Ya existe una comunidad con los mismos datos"
            db.rollback()

        if snmp_community.id is not None and snmp_community.id > 0:
            return jsonify(utils.as_dict(snmp_community)), HTTPStatus.OK.value
        else:
            return jsonify({"error": f"{error_str}"}), HTTPStatus.UNPROCESSABLE_ENTITY.value


@app.route("/api/v1/user/add", methods=["POST"])
def api_user_add() -> tuple[Response, int]:
    json_data: dict[str, str] | None = request.json
    error: dict[str, str] = {}
    data: dict[str, str] = {}

    if isinstance(json_data, dict):
        data = json_data

    if "username" not in data:
        error["missing_username"] = "Falta el usuario"

    if "mail" not in data:
        error["missing_mail"] = "Falta el correo del usuario"

    if "password" not in data:
        error["missing_password"] = "Falta la contraseña del usuario"

    if "area" not in data:
        error["missing_area"] = "Falta el area del usuario"

    if error:
        return jsonify(error), HTTPStatus.BAD_REQUEST.value

    new_user: models.User = models.User(
        username=data["username"], mail=data["mail"], passwd=generate_passwd(data["password"]), area_id=int(data["area"])
    )

    with app.app_context():
        db.add(new_user)

        error_str: str = ""

        try:
            db.commit()
        except IntegrityError as err:
            if "Duplicate entry" in err.orig.args[1]:
                error_str = "Ya existe un usuario con los mismos datos"
            db.rollback()

        if new_user.id is not None and new_user.id > 0:
            return jsonify(utils.as_dict(new_user)), HTTPStatus.CREATED.value
        else:
            return jsonify({"error": f"{error_str}"}), HTTPStatus.UNPROCESSABLE_ENTITY.value


@app.route("/api/v1/user/delete/<int:id_user>", methods=["DELETE"])
def api_user_delete(id_user: int) -> tuple[Response, int]:
    user: models.User

    with app.app_context():
        user = db.query(models.User).filter(models.User.id == id_user).first()

    if not id_user or user is None:
        return jsonify({"error": "User not found"}), HTTPStatus.NOT_FOUND.value
    else:
        with app.app_context():
            db.delete(user)
            error_str: str = ""

            try:
                db.commit()
            except IntegrityError as err:
                error_str = err.orig.args[1]
                db.rollback()

        if error_str:
            return jsonify({"error": f"{error_str}"}), HTTPStatus.UNPROCESSABLE_ENTITY.value
        else:
            return jsonify({"success": f"User [{id_user}] was successfully deleted"}), HTTPStatus.OK.value


@app.route("/api/v1/user/update", methods=["PUT"])
def api_user_update() -> tuple[Response, int]:
    json_data: dict[str, str] | None = request.json
    error: dict[str, str] = {}
    data: dict[str, str] = {}

    if isinstance(json_data, dict):
        data = json_data

    if "id" not in data:
        error["missing_id"] = "Falta el id del usuario"

    if "username" not in data:
        error["missing_username"] = "Falta el usuario"

    if "mail" not in data:
        error["missing_mail"] = "Falta el correo del usuario"

    if "password" not in data:
        error["missing_password"] = "Falta la contraseña del usuario"

    if "area" not in data:
        error["missing_area"] = "Falta el area del usuario"

    if error:
        return jsonify(error), HTTPStatus.BAD_REQUEST.value

    user: models.User | None

    with app.app_context():
        user = db.query(models.User).filter(models.User.id == int(data["id"])).first()

    if user is None:
        return jsonify({"error": "User not found"}), HTTPStatus.NOT_FOUND.value

    setattr(user, "username", data["username"])
    setattr(user, "mail", data["mail"])
    if data["password"]:
        setattr(user, "passwd", generate_passwd(data["password"]))
    setattr(user, "area_id", data["area"])

    with app.app_context():
        db.merge(user)
        error_str: str = ""

        try:
            db.commit()
        except IntegrityError as err:
            if "Duplicate entry" in err.orig.args[1]:
                error_str = "Ya existe un usuario con los mismos datos"
            db.rollback()

        if user.id is not None and user.id > 0:
            return jsonify(utils.as_dict(user)), HTTPStatus.OK.value
        else:
            return jsonify({"error": f"{error_str}"}), HTTPStatus.UNPROCESSABLE_ENTITY.value


@app.route("/api/v1/snmp/get/name/<int:id_device>/<int:id_community>", methods=["GET"])
def api_get_snmp_name(id_device: int, id_community: int) -> tuple[Response, int]:
    device: models.Device | None
    device = db.query(models.Device).filter(models.Device.id == id_device).first()

    snmp_community: models.SNMPCredentials | None
    snmp_community = db.query(models.SNMPCredentials).filter(models.SNMPCredentials.id == id_community).first()

    info_snmp: tuple[bool, str | None, str | None]

    if not device or not snmp_community:
        info_snmp = False, "Device or community not found", ""
    else:
        info_snmp = snmp.get_snmp_name(
            snmp_community.data_parsed["info"]["community"], device.get_ip_str()  # type: ignore
        )

    if info_snmp[0]:
        return jsonify({"success": {"name": info_snmp[2], "oid": info_snmp[1]}}), HTTPStatus.OK.value
    else:
        return jsonify({"fail": {"reason": info_snmp[1]}}), HTTPStatus.NOT_FOUND.value


# endregion


# region All page


@app.route("/")
@login_required
def index() -> str:
    return render_template("index.html")


@app.route("/information")
def information() -> str:
    return ""


@app.route("/areas")
@login_required
def areas() -> str:
    area_list: list[models.Area] = db.query(models.Area).all()
    return render_template("pages/area/index.html", areas=area_list)


@app.route("/areas/edit/<int:id_area>", methods=["GET"])
@login_required
def areas_edit(id_area: int) -> str | Response:
    area: models.Area | None

    with app.app_context():
        area = db.query(models.Area).filter(models.Area.id == id_area).first()

        if area is None:
            return redirect(url_for("areas"))

    return render_template("pages/area/edit.html", area=area)


@app.route("/areas/show/<int:id_area>", methods=["GET"])
@login_required
def areas_show(id_area: int) -> str | Response:
    area: models.Area | None

    with app.app_context():
        area = db.query(models.Area).filter(models.Area.id == id_area).first()

        if area is None:
            return redirect(url_for("areas"))
        # else:

        # for _device in area.devices:
        #     _device.snmp_credential

        # for _user in area.users:
        #     _user.user_type

    return render_template("pages/area/show.html", area=area)


@app.route("/devices")
@login_required
def devices() -> str:
    device_list: list[models.Device] = db.query(models.Device).all()
    area_list: list[models.Area] = db.query(models.Area).all()
    snmp_community_list: list[models.SNMPCredentials] = db.query(models.SNMPCredentials).all()

    # for _device in device_list:
    #    _device.snmp_credential

    snmp_version: dict[str, Any] = {version.name: version.value for version in models.SNMPVersion}
    mask_address: list[dict[str, Any]] = utils.get_table_mask_address()

    return render_template(
        "pages/device/index.html",
        devices=device_list,
        mask_address=mask_address,
        areas=area_list,
        snmp_communities=snmp_community_list,
        snmp_version=snmp_version,
    )


@app.route("/devices/edit/<int:id_device>", methods=["GET"])
@login_required
def devices_edit(id_device: int) -> str | Response:
    device: models.Device | None

    with app.app_context():
        device = db.query(models.Device).filter(models.Device.id == id_device).first()
        if device is None:
            return redirect(url_for("devices"))

    area_list: list[models.Area] = db.query(models.Area).all()
    snmp_community_list: list[models.SNMPCredentials] = db.query(models.SNMPCredentials).all()

    snmp_version: dict[str, Any] = {version.name: version.value for version in models.SNMPVersion}
    mask_address: list[dict[str, Any]] = utils.get_table_mask_address()

    return render_template(
        "pages/device/edit.html",
        device=device,
        mask_address=mask_address,
        areas=area_list,
        snmp_communities=snmp_community_list,
        snmp_version=snmp_version,
    )


@app.route("/devices/show/<int:id_device>", methods=["GET"])
@login_required
def devices_show(id_device: int) -> str | Response:
    device: models.Device | None

    with app.app_context():
        device = db.query(models.Device).filter(models.Device.id == id_device).first()

        if device is None:
            return redirect(url_for("snmp_communities"))
        # else:
        #     device.area
        #     device.snmp_credential

    return render_template("pages/device/show.html", device=device)


@app.route("/snmp_communities")
@login_required
def snmp_communities() -> str:
    snmp_community_list: list[models.SNMPCredentials] = db.query(models.SNMPCredentials).all()
    snmp_version: dict[str, Any] = {version.name: version.value for version in models.SNMPVersion}

    return render_template(
        "pages/snmp_community/index.html", snmp_communities=snmp_community_list, snmp_version=snmp_version
    )


@app.route("/snmp_communities/edit/<int:id_community>", methods=["GET"])
@login_required
def snmp_communities_edit(id_community: int) -> str | Response:
    snmp_community: models.SNMPCredentials | None

    with app.app_context():
        snmp_community = db.query(models.SNMPCredentials).filter(models.SNMPCredentials.id == id_community).first()
        if snmp_community is None:
            return redirect(url_for("snmp_communities"))

    snmp_version: dict[str, Any] = {version.name: version.value for version in models.SNMPVersion}

    return render_template("pages/snmp_community/edit.html", community=snmp_community, snmp_version=snmp_version)


@app.route("/snmp_communities/show/<int:id_community>", methods=["GET"])
@login_required
def snmp_communities_show(id_community: int) -> str | Response:
    snmp_community: models.SNMPCredentials | None

    with app.app_context():
        snmp_community = db.query(models.SNMPCredentials).filter(models.SNMPCredentials.id == id_community).first()

        if snmp_community is None:
            return redirect(url_for("snmp_communities"))
        # else:
        # for _device in snmp_community.devices:
        #     _device.area
        # _device.snmp_credential

    return render_template("pages/snmp_community/show.html", community=snmp_community)


@app.route("/users")
@login_required
def users() -> str:
    user_list: list[models.User] = db.query(models.User).all()
    area_list: list[models.Area] = db.query(models.Area).all()

    return render_template("pages/user/index.html", users=user_list, areas=area_list)


@app.route("/users/edit/<int:id_user>", methods=["GET"])
@login_required
def users_edit(id_user: int) -> str | Response:
    user: models.User | None

    with app.app_context():
        user = db.query(models.User).filter(models.User.id == id_user).first()

        if user is None:
            return redirect(url_for("users"))
        # else:
        #     user.area

    area_list: list[models.Area] = db.query(models.Area).all()

    return render_template("pages/user/edit.html", user=user, areas=area_list)


# @app.route('/users/show/<int:id_user>', methods=['GET'])
# @login_required
# def users_show(id_user: int):
#     user: models.User

#     with app.app_context():
#         user = models.User.query.filter_by(id=id_user).first()

#         if user is None:
#             return redirect(url_for('users'))

#     return render_template('pages/user/show.html', user=user)
# endregion


@app.errorhandler(HTTPStatus.NOT_FOUND.value)
def page_not_found(_error: Exception) -> tuple[str, int]:
    return render_template("errors/404.html"), HTTPStatus.NOT_FOUND.value


if __name__ == "__main__":
    app.run()
