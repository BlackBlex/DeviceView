# from flask_sqlalchemy import SQLAlchemy

# # Iniciamos SQLAlchemy con el objeto Flask
# db = SQLAlchemy()

from os import getenv

from dotenv import load_dotenv
from sqlalchemy.engine import create_engine
from sqlalchemy.engine.base import Engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.decl_api import DeclarativeMeta

load_dotenv()

SQLALCHEMY_DATABASE_URL = getenv("SQLALCHEMY_DATABASE_URI", "no_database")

engine: Engine = create_engine(SQLALCHEMY_DATABASE_URL)  # , connect_args={"check_same_thread": False}

SessionLocal: sessionmaker = sessionmaker(autocommit=False, autoflush=False, bind=engine)

BaseModel: DeclarativeMeta = declarative_base()
