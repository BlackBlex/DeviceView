import enum
import ipaddress
import json
import logging
from datetime import datetime
from typing import TYPE_CHECKING, Any, TypeVar

from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import Column, ForeignKey
from sqlalchemy.sql.sqltypes import BigInteger, Integer, String
from sqlalchemy.types import Boolean, DateTime

from www.database import BaseModel

if TYPE_CHECKING:
    hybrid_property = property
    from sqlalchemy.sql.type_api import TypeEngine

    T = TypeVar("T")

    class Enum(TypeEngine[T]):
        def __init__(self, _enum: type[T], **_kwargs: Any) -> None:
            ...

else:
    from sqlalchemy import Enum
    from sqlalchemy.ext.hybrid import hybrid_property

logging.basicConfig(format="%(asctime)s - %(levelname)s > %(name)s : %(message)s", level=logging.DEBUG)
logger = logging.getLogger(__name__)


class SNMPVersion(enum.Enum):
    V1 = 1
    V2 = 2
    V3 = 3


class Device(BaseModel):  # type: ignore
    __tablename__ = "devices"

    id: Column[int] = Column(Integer, primary_key=True, nullable=False)
    ip_address: Column[int] = Column(
        BigInteger, default=int(ipaddress.IPv4Address("127.0.0.1")), unique=True, nullable=False
    )
    mask_address: Column[int] = Column(BigInteger, default=int(ipaddress.IPv4Address("255.255.255.0")), nullable=False)
    name: Column[str] = Column(String(35), nullable=False)
    snmp_credential_id: Column[int] = Column(Integer, ForeignKey("snmp_credentials.id"), nullable=False)
    area_id: Column[int] = Column(Integer, ForeignKey("areas.id"), nullable=False)

    active: Column[bool] = Column(Boolean, default=True, nullable=False)
    created: Column[datetime] = Column(DateTime, nullable=False, default=datetime.now)
    modified: Column[datetime] = Column(DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)

    @hybrid_property
    def ip_address_str(self) -> str:
        return str(ipaddress.IPv4Address(self.ip_address))

    @hybrid_property
    def mask_address_str(self) -> str:
        return str(ipaddress.IPv4Address(self.mask_address))

    @hybrid_property
    def cidr(self) -> int:
        return ipaddress.IPv4Network(f"{self.get_ip_str()}/{self.get_mask_str()}", strict=False).prefixlen

    def get_ip_str(self) -> str:
        return str(ipaddress.IPv4Address(self.ip_address))

    def get_mask_str(self) -> str:
        return str(ipaddress.IPv4Address(self.mask_address))


class User(BaseModel):  # type:ignore
    __tablename__ = "users"

    id: Column[int] = Column(Integer, primary_key=True, nullable=False)
    username: Column[str] = Column(String(35), unique=True, nullable=False)
    mail: Column[str] = Column(String(256), unique=True, nullable=False)
    passwd: Column[str] = Column(String(128), nullable=False)
    area_id: Column[int] = Column(Integer, ForeignKey("areas.id"), nullable=False)
    user_type_id: Column[int] = Column(Integer, ForeignKey("user_types.id"), default=2, nullable=False)

    active: Column[bool] = Column(Boolean, default=True, nullable=False)
    created: Column[datetime] = Column(DateTime, nullable=False, default=datetime.now)
    modified: Column[datetime] = Column(DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)


class Area(BaseModel):  # type: ignore
    __tablename__ = "areas"

    id: Column[int] = Column(Integer, primary_key=True, nullable=False)
    name: Column[str] = Column(String(35), unique=True, nullable=False)
    active: Column[bool] = Column(Boolean, default=True, nullable=False)
    created: Column[datetime] = Column(DateTime, nullable=False, default=datetime.now)
    modified: Column[datetime] = Column(DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)
    devices = relationship("Device", backref="area", lazy=True)
    users = relationship("User", backref="area", lazy=True)


class SNMPCredentials(BaseModel):  # type:ignore
    __tablename__ = "snmp_credentials"

    id: Column[int] = Column(Integer, primary_key=True, nullable=False)
    data: Column[str] = Column(String(500), default="{}", nullable=False)
    version: Column[SNMPVersion] = Column(Enum(SNMPVersion), default=SNMPVersion.V1, nullable=False)
    active: Column[bool] = Column(Boolean, default=True, nullable=False)
    created: Column[datetime] = Column(DateTime, nullable=False, default=datetime.now)
    modified: Column[datetime] = Column(DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)
    devices = relationship("Device", backref="snmp_credential", lazy=True)

    @hybrid_property
    def data_parsed(self) -> dict[str, Any]:
        return json.loads(str(self.data))


class UserType(BaseModel):  # type:ignore
    __tablename__ = "user_types"

    id: Column[int] = Column(Integer, primary_key=True, nullable=False)
    name: Column[str] = Column(String(40), nullable=False)
    active: Column[bool] = Column(Boolean, default=True, nullable=False)
    created: Column[datetime] = Column(DateTime, nullable=False, default=datetime.now)
    modified: Column[datetime] = Column(DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)
    users = relationship("User", backref="user_type", lazy=True)
