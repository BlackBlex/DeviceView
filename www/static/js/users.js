$(document).ready(function () {
    // #region Events
    ApplicationGlobal.on_area_added = function (json) {
        $("#return-device-from-area").click();
        $("#area").append(new Option(json["name"], json["id"], true, true));
    };

    ApplicationGlobal.on_user_added = function (json) {
        location.reload();
    };
    // #endregion

    // #region Setup elements
    $("#alert-container-error-user").hide();

    $("#return-device-from-area").removeAttr("hidden");
    $("#close-area").hide();
    // #endregion

    // #region Buttons
    $("#return-user").on("click", function () {
        history.back();
    });

    $(".btn-danger").on("click", function () {
        let delete_url = $(this).attr("data-url");

        $.ajax({
            url: delete_url,
            type: "DELETE",
        })
            .done(function (data) {
                let json = data;

                if (json["success"]) {
                    sessionStorage.setItem("deviceDeleted", "true");
                    location.reload();
                }
            })
            .fail(function (jqXHR) {
                let json = jqXHR.responseJSON;
            });
    });
    // #endregion

    // #region Submits forms
    $("#add-user-form").submit(function (event) {
        event.preventDefault();

        var data = {
            username: $("#username").val(),
        };

        $("#alert-container-error-user").hide();
        $("#alert-text-error-user").text("");

        if (data["username"].trim().length === 0) {
            $("#alert-text-error-user").text(
                "No puede quedar en blanco el usuario"
            );
            $("#alert-container-error-user").show();
        } else {
            data["mail"] = $("#mail").val();
            data["password"] = $("#password").val();
            data["area"] = $("#area").val();

            let url_ajax = $("#add-user-url").val();
            let url_type = "POST";

            if ($("#user-id").length) {
                url_ajax = $("#update-user-url").val();
                url_type = "PUT";
                data["id"] = $("#user-id").val();
            }

            $.ajax({
                type: url_type,
                url: url_ajax,
                data: JSON.stringify(data),
                contentType: "application/json",
                dataType: "json",
                encode: true,
            })
                .done(function (data) {
                    let json = data;

                    if (json["id"]) {
                        ApplicationGlobal.on_user_added(json);
                    }
                })
                .fail(function (jqXHR) {
                    let json = jqXHR.responseJSON;

                    if (json["missing_id"]) {
                        $("#alert-text-error-user").text(json["missing_id"]);
                    } else if (json["missing_username"]) {
                        $("#alert-text-error-user").text(
                            json["missing_username"]
                        );
                    } else if (json["missing_mail"]) {
                        $("#alert-text-error-user").text(json["missing_mail"]);
                    } else if (json["missing_password"]) {
                        $("#alert-text-error-user").text(
                            json["missing_password"]
                        );
                    } else if (json["missing_area"]) {
                        $("#alert-text-error-user").text(json["missing_area"]);
                    } else if (json["error"]) {
                        $("#alert-text-error-user").text(json["error"]);
                    }

                    if ($("#alert-text-error-user").text().length !== 0) {
                        $("#alert-container-error-user").show();
                    }
                });
        }
    });
    // #endregion
});
