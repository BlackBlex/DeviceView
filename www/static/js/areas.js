$(document).ready(function () {
    // #region Events
    ApplicationGlobal.on_area_added = function (json) {
        location.reload();
    };
    // #endregion

    // #region Setup elements
    $("#alert-container-error-area").hide();
    // #endregion

    // #region Buttons
    $("#return-area").on("click", function () {
        history.back();
    });

    $(".btn-danger").on("click", function () {
        let delete_url = $(this).attr("data-url");

        $.ajax({
            url: delete_url,
            type: "DELETE",
        })
            .done(function (data) {
                let json = data;

                if (json["success"]) {
                    sessionStorage.setItem("communityDeleted", "true");
                    location.reload();
                }
            })
            .fail(function (jqXHR) {
                let json = jqXHR.responseJSON;
            });
    });
    // #endregion

    // #region Submits forms
    $("#add-area-form").submit(function (event) {
        event.preventDefault();

        var data = {
            name: $("#area-name").val(),
        };

        $("#alert-container-error-area").hide();
        $("#alert-text-error-area").text("");

        if (data["name"].trim().length === 0) {
            $("#alert-text-error-area").text(
                "No puede quedar en blanco el nombre"
            );
            $("#alert-container-error-area").show();
        } else {
            let url_ajax = $("#add-area-url").val();
            let url_type = "POST";

            if ($("#area-id").length) {
                url_ajax = $("#update-area-url").val();
                url_type = "PUT";
                data["id"] = $("#area-id").val();
            }

            $.ajax({
                type: url_type,
                url: url_ajax,
                data: JSON.stringify(data),
                contentType: "application/json",
                dataType: "json",
                encode: true,
            })
                .done(function (data) {
                    let json = data;

                    if (json["id"]) {
                        ApplicationGlobal.on_area_added(json);
                    }
                })
                .fail(function (jqXHR) {
                    let json = jqXHR.responseJSON;

                    if (json["missing_id"]) {
                        $("#alert-text-error-area").text(json["missing_id"]);
                    } else if (json["missing_name"]) {
                        $("#alert-text-error-area").text(json["missing_name"]);
                    } else if (json["error"]) {
                        $("#alert-text-error-area").text(json["error"]);
                    }

                    if ($("#alert-text-error-area").text().length !== 0) {
                        $("#alert-container-error-area").show();
                    }
                });
        }
    });
    // #endregion
});
