function toggleDeviceMask() {
    $("#device-mask").toggle();
    $("#device-mask").prop("required", !$("#device-mask").prop("required"));
    $("#device-cidr").toggle();
    $("#device-cidr").prop("required", !$("#device-cidr").prop("required"));
}

$(document).ready(function () {
    // #region Events
    ApplicationGlobal.on_area_added = function (json) {
        $("#return-device-from-area").click();
        $("#device-area").append(
            new Option(json["name"], json["id"], true, true)
        );
    };

    ApplicationGlobal.on_snmp_community_added = function (json) {
        $("#return-device-from-snmp-community").click();
        $("#device-snmp-community").append(
            new Option(
                JSON.parse(json["data"])["name"] +
                    " (V" +
                    json["version"] +
                    ")",
                json["id"],
                true,
                true
            )
        );
    };

    ApplicationGlobal.on_device_added = function (json) {
        location.reload();
    };
    // #endregion

    // #region Setup elements
    $("#device-mask").hide();
    toggleDeviceMask();
    $("#alert-container-error-device").hide();

    $("#return-device-from-area").removeAttr("hidden");
    $("#close-area").hide();
    $("#return-device-from-snmp-community").removeAttr("hidden");
    $("#close-snmp-community").hide();
    // #endregion

    // #region Buttons
    $("#return-device").on("click", function () {
        history.back();
    });

    $(".btn-danger").on("click", function () {
        let delete_url = $(this).attr("data-url");

        $.ajax({
            url: delete_url,
            type: "DELETE",
        })
            .done(function (data) {
                let json = data;

                if (json["success"]) {
                    sessionStorage.setItem("deviceDeleted", "true");
                    location.reload();
                }
            })
            .fail(function (jqXHR) {
                let json = jqXHR.responseJSON;
            });
    });
    // #endregion

    // #region Select elements
    // $("#device-mask").on("change", function () {
    //     if ($(this).is(":visible")) {
    //         alert(this.value);
    //     }
    // });

    // $("#device-cidr").on("change", function () {
    //     if ($(this).is(":visible")) {
    //         alert(this.value);
    //     }
    // });
    // #endregion

    // #region Checks & radios
    $("input[type=radio][name=type_mask_ip]").on("change", function () {
        switch ($(this).val()) {
            case "mask_address":
                $("#device-mask").show();
                $("#device-mask").prop("required", true);
                $("#device-cidr").hide();
                $("#device-cidr").prop("required", false);
                break;
            case "cidr":
                $("#device-cidr").show();
                $("#device-cidr").prop("required", true);
                $("#device-mask").hide();
                $("#device-mask").prop("required", false);
                break;
        }
    });
    // #endregion

    // #region Submits forms
    $("#add-device-form").submit(function (event) {
        event.preventDefault();

        var data = {
            name: $("#device-name").val(),
        };

        $("#alert-container-error-device").hide();
        $("#alert-text-error-device").text("");

        if (data["name"].trim().length === 0) {
            $("#alert-text-error-device").text(
                "No puede quedar en blanco el nombre"
            );
            $("#alert-container-error-device").show();
        } else {
            data["ip"] = $("#device-ip").val();

            if ($("#device-cidr").is(":visible")) {
                data["cidr"] = $("#device-cidr").val();
            } else {
                data["mask"] = $("#device-mask").val();
            }

            data["snmp_community"] = $("#device-snmp-community").val();
            data["area"] = $("#device-area").val();

            let url_ajax = $("#add-device-url").val();
            let url_type = "POST";

            if ($("#device-id").length) {
                url_ajax = $("#update-device-url").val();
                url_type = "PUT";
                data["id"] = $("#device-id").val();
            }

            $.ajax({
                type: url_type,
                url: url_ajax,
                data: JSON.stringify(data),
                contentType: "application/json",
                dataType: "json",
                encode: true,
            })
                .done(function (data) {
                    let json = data;

                    if (json["id"]) {
                        ApplicationGlobal.on_device_added(json);
                    }
                })
                .fail(function (jqXHR) {
                    let json = jqXHR.responseJSON;

                    if (json["missing_id"]) {
                        $("#alert-text-error-device").text(json["missing_id"]);
                    } else if (json["missing_name"]) {
                        $("#alert-text-error-device").text(
                            json["missing_name"]
                        );
                    } else if (json["missing_ip_address"]) {
                        $("#alert-text-error-device").text(
                            json["missing_ip_address"]
                        );
                    } else if (json["missing_mask_cidr"]) {
                        $("#alert-text-error-device").text(
                            json["missing_mask_cidr"]
                        );
                    } else if (json["snmp_community"]) {
                        $("#alert-text-error-device").text(
                            json["snmp_community"]
                        );
                    } else if (json["missing_area"]) {
                        $("#alert-text-error-device").text(
                            json["missing_area"]
                        );
                    } else if (json["error"]) {
                        $("#alert-text-error-device").text(json["error"]);
                    }

                    if ($("#alert-text-error-device").text().length !== 0) {
                        $("#alert-container-error-device").show();
                    }
                });
        }
    });
    // #endregion
});
