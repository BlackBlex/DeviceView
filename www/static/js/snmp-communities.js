function toggleCommunity() {
    $("#snmp-community-community-div").toggle();
    $("#snmp-community-community").prop(
        "required",
        !$("#snmp-community-community").prop("required")
    );
}

function toggleUserSNMP() {
    $("#snmp-community-user-div").toggle();
    $("#snmp-community-user").prop(
        "required",
        !$("#snmp-community-user").prop("required")
    );
}

function toggleContextSNMP() {
    $("#snmp-community-context-div").toggle();
    // $("#snmp-community-context").prop(
    //     "required",
    //     !$("#snmp-community-context").prop("required")
    // );
}

function toggleAuthSNMP() {
    $("#snmp-community-auth-div").toggle();
    $("#snmp-community-auth-value").prop(
        "required",
        !$("#snmp-community-auth-value").prop("required")
    );
}

function togglePrivSNMP() {
    $("#snmp-community-priv-div").toggle();
    $("#snmp-community-priv-value").prop(
        "required",
        !$("#snmp-community-priv-value").prop("required")
    );
}

$(document).ready(function () {
    // #region Events
    ApplicationGlobal.on_snmp_community_added = function (json) {
        location.reload();
    };
    // #endregion

    // #region Setup elements
    toggleCommunity();
    toggleUserSNMP();
    toggleContextSNMP();

    toggleAuthSNMP();
    // $("#snmp-community-auth-type").prop("required", false);
    togglePrivSNMP();
    // $("#snmp-community-priv-type").prop("required", false);

    $("#alert-container-error-snmp-community").hide();
    // #endregion

    // #region Buttons
    $("#return-snmp-community").on("click", function () {
        history.back();
    });

    $(".btn-danger").on("click", function () {
        let delete_url = $(this).attr("data-url");

        $.ajax({
            url: delete_url,
            type: "DELETE",
        })
            .done(function (data) {
                let json = data;

                if (json["success"]) {
                    sessionStorage.setItem("communityDeleted", "true");
                    location.reload();
                }
            })
            .fail(function (jqXHR) {
                let json = jqXHR.responseJSON;
            });
    });
    // #endregion

    // #region Select elements
    $("#snmp-community-version").on("change", function () {
        if ($("#snmp-community-community-div").is(":visible")) {
            toggleCommunity();
        }
        if ($("#snmp-community-user-div").is(":visible")) {
            toggleUserSNMP();
            toggleAuthSNMP();
            toggleContextSNMP();
            togglePrivSNMP();
        }

        switch (this.value) {
            case "1":
            case "2":
                toggleCommunity();
                break;
            case "3":
                toggleUserSNMP();
                toggleAuthSNMP();
                toggleContextSNMP();
                togglePrivSNMP();
                // $("#snmp-community-user-div").show();
                // $("#snmp-community-context-div").show();
                // $("#snmp-community-auth-div").show();
                // $("#snmp-community-priv-div").show();

                // $("#snmp-community-user").prop("required", true);
                // $("#snmp-community-context").prop("required", true);
                // $("#snmp-community-auth-value").prop("required", true);
                // $("#snmp-community-auth-type").prop("required", true);
                // $("#snmp-community-priv-value").prop("required", true);
                // $("#snmp-community-priv-type").prop("required", true);
                break;
        }
    });
    // #endregion

    // #region Checks & radios
    $("#snmp-community-auth-check").change(function () {
        let checked = $(this).is(":not(:checked)");
        $("#snmp-community-auth-value").prop("disabled", checked);
        $("#snmp-community-auth-type-md5").prop("disabled", checked);
        $("#snmp-community-auth-type-sha").prop("disabled", checked);
    });

    $("#snmp-community-priv-check").change(function () {
        let checked = $(this).is(":not(:checked)");
        $("#snmp-community-priv-value").prop("disabled", checked);
        $("#snmp-community-priv-type-aes").prop("disabled", checked);
        $("#snmp-community-priv-type-des").prop("disabled", checked);
    });
    // #endregion

    // #region Submits forms
    $("#add-snmp-community-form").submit(function (event) {
        event.preventDefault();

        let name = $("#snmp-community-name").val();
        var data = {};

        $("#alert-container-error-snmp-community").hide();
        $("#alert-text-error-snmp-community").text("");

        if (name.trim().length === 0) {
            $("#alert-text-error-snmp-community").text(
                "No puede quedar en blanco el nombre"
            );
            $("#alert-container-error-snmp-community").show();
        } else {
            data["snmp"] = {};
            data["snmp"][name] = {};

            data["version"] = $("#snmp-community-version").val();

            switch (data["version"]) {
                case "1":
                case "2":
                    data["snmp"][name]["community"] = $(
                        "#snmp-community-community"
                    ).val();
                    break;
                case "3":
                    data["snmp"][name]["user"] = $(
                        "#snmp-community-user"
                    ).val();
                    data["snmp"][name]["context"] = $(
                        "#snmp-community-context"
                    ).val();

                    if ($("#snmp-community-auth-check").is(":checked")) {
                        data["snmp"][name]["auth_key"] = $(
                            "#snmp-community-auth-value"
                        ).val();
                        data["snmp"][name]["auth_type"] = $(
                            "input[name=snmp-community-auth-type]:checked",
                            "#add-snmp-community-form"
                        ).val();
                    }
                    if ($("#snmp-community-priv-check").is(":checked")) {
                        data["snmp"][name]["priv_key"] = $(
                            "#snmp-community-priv-value"
                        ).val();
                        data["snmp"][name]["priv_type"] = $(
                            "input[name=snmp-community-priv-type]:checked",
                            "#add-snmp-community-form"
                        ).val();
                    }

                    if (
                        data["snmp"][name]["priv_key"] &&
                        !data["snmp"][name]["auth_key"]
                    ) {
                        $("#alert-text-error-snmp-community").text(
                            "No puedes tener solamente el protocolo de cifrado"
                        );
                        $("#alert-container-error-snmp-community").show();

                        data = {};
                    }

                    break;
            }
        }

        let url_ajax = $("#add-snmp-community-url").val();
        let url_type = "POST";

        if ($("#snmp-community-id").length) {
            url_ajax = $("#update-snmp-community-url").val();
            url_type = "PUT";
            data["id"] = $("#snmp-community-id").val();
        }

        if (data["snmp"]) {
            $.ajax({
                type: url_type,
                url: url_ajax,
                data: JSON.stringify(data),
                contentType: "application/json",
                dataType: "json",
                encode: true,
            })
                .done(function (data) {
                    let json = data;

                    if (json["id"]) {
                        ApplicationGlobal.on_snmp_community_added(json);
                    }
                })
                .fail(function (jqXHR) {
                    let json = jqXHR.responseJSON;

                    if (json["missing_id"]) {
                        $("#alert-text-error-snmp-community").append(
                            json["missing_id"]
                        );
                    } else if (json["missing_snmp"]) {
                        $("#alert-text-error-snmp-community").append(
                            json["missing_snmp"]
                        );
                    } else if (json["missing_snmp_type"]) {
                        $("#alert-text-error-snmp-community").append(
                            json["missing_snmp_type"]
                        );
                    } else if (json["missing_version"]) {
                        $("#alert-text-error-snmp-community").append(
                            json["missing_version"]
                        );
                    } else if (json["missing_community"]) {
                        $("#alert-text-error-snmp-community").append(
                            json["missing_community"]
                        );
                    } else if (json["missing_user"]) {
                        $("#alert-text-error-snmp-community").append(
                            json["missing_user"]
                        );
                    } else if (json["error"]) {
                        $("#alert-text-error-snmp-community").append(
                            json["error"]
                        );
                    }

                    if (
                        $("#alert-text-error-snmp-community").text().length !==
                        0
                    ) {
                        $("#alert-container-error-snmp-community").show();
                    }
                });
        } else {
            console.log("error");
        }
    });
    // #endregion
});
