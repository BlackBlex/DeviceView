$(document).ready(function () {
    if ($("#alert-text-error").text().trim().length === 0) {
        $("#alert-container-error").hide();
    }

    if ($("#alert-text-success").text().trim().length === 0) {
        $("#alert-container-success").hide();
    }

    $("#login-form").submit(function (event) {
        var data = {
            mail: $("#mail").val(),
            passwd: $("#passwd").val(),
        };

        $("#alert-container-error").hide();
        $("#alert-container-success").hide();
        $("#alert-text-error").text("");
        $("#alert-text-success").text("");

        $.ajax({
            type: "POST",
            url: "login",
            data: JSON.stringify(data),
            contentType: "application/json",
            dataType: "json",
            encode: true,
        }).done(function (data) {
            var json = data;

            if (json["missing_mail"]) {
                $("#alert-text-error").text(json["missing_mail"]);
            } else if (json["missing_passwd"]) {
                $("#alert-text-error").text(json["missing_passwd"]);
            } else if (json["missing_user_passwd"]) {
                $("#alert-text-error").text(json["missing_user_passwd"]);
            }

            if ($("#alert-text-error").text().length !== 0) {
                $("#alert-container-error").show();
            } else {
                if (json["success"]) {
                    window.location.href = $("#next").val();
                }
            }
        });

        event.preventDefault();
    });
});
