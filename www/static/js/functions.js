function convertTime(dateTime) {
    return moment(new Date(dateTime)).format("DD MMMM YYYY, h:mm:ss a");
}

$(document).ready(function () {
    moment.locale("es-mx");

    const tooltipTriggerList = document.querySelectorAll(
        '[data-bs-toggle="tooltip"]'
    );

    const tooltipList = [...tooltipTriggerList].map(
        (tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl)
    );
});
