import enum
import ipaddress
from typing import Any

from www.database import BaseModel

__mask_address: list[dict[str, Any]] = [
    {"cidr": 32, "mask_address": "255.255.255.255", "wild_card": "0.0.0.0", "no_host": 1, "no_ip": 1},
    {"cidr": 31, "mask_address": "255.255.255.254", "wild_card": "0.0.0.1", "no_host": 2, "no_ip": 2},
    {"cidr": 30, "mask_address": "255.255.255.252", "wild_card": "0.0.0.3", "no_host": 4, "no_ip": 2},
    {"cidr": 29, "mask_address": "255.255.255.248", "wild_card": "0.0.0.7", "no_host": 8, "no_ip": 6},
    {"cidr": 28, "mask_address": "255.255.255.240", "wild_card": "0.0.0.15", "no_host": 16, "no_ip": 14},
    {"cidr": 27, "mask_address": "255.255.255.224", "wild_card": "0.0.0.31", "no_host": 32, "no_ip": 30},
    {"cidr": 26, "mask_address": "255.255.255.192", "wild_card": "0.0.0.63", "no_host": 64, "no_ip": 62},
    {"cidr": 25, "mask_address": "255.255.255.128", "wild_card": "0.0.0.127", "no_host": 128, "no_ip": 126},
    {"cidr": 24, "mask_address": "255.255.255.0", "wild_card": "0.0.0.255", "no_host": 256, "no_ip": 254},
    {"cidr": 23, "mask_address": "255.255.254.0", "wild_card": "0.0.1.255", "no_host": 512, "no_ip": 510},
    {"cidr": 22, "mask_address": "255.255.252.0", "wild_card": "0.0.3.255", "no_host": 1024, "no_ip": 1022},
    {"cidr": 21, "mask_address": "255.255.248.0", "wild_card": "0.0.7.255", "no_host": 2048, "no_ip": 2046},
    {"cidr": 20, "mask_address": "255.255.240.0", "wild_card": "0.0.15.255", "no_host": 4096, "no_ip": 4094},
    {"cidr": 19, "mask_address": "255.255.224.0", "wild_card": "0.0.31.255", "no_host": 8192, "no_ip": 8190},
    {"cidr": 18, "mask_address": "255.255.192.0", "wild_card": "0.0.63.255", "no_host": 16384, "no_ip": 16382},
    {"cidr": 17, "mask_address": "255.255.128.0", "wild_card": "0.0.127.255", "no_host": 32768, "no_ip": 32766},
    {"cidr": 16, "mask_address": "255.255.0.0", "wild_card": "0.0.255.255", "no_host": 65536, "no_ip": 65534},
    {"cidr": 15, "mask_address": "255.254.0.0", "wild_card": "0.1.255.255", "no_host": 131072, "no_ip": 131070},
    {"cidr": 14, "mask_address": "255.252.0.0", "wild_card": "0.3.255.255", "no_host": 262144, "no_ip": 262142},
    {"cidr": 13, "mask_address": "255.248.0.0", "wild_card": "0.7.255.255", "no_host": 524288, "no_ip": 524286},
    {"cidr": 12, "mask_address": "255.240.0.0", "wild_card": "0.15.255.255", "no_host": 1048576, "no_ip": 1048574},
    {"cidr": 11, "mask_address": "255.224.0.0", "wild_card": "0.31.255.255", "no_host": 2097152, "no_ip": 2097150},
    {"cidr": 10, "mask_address": "255.192.0.0", "wild_card": "0.63.255.255", "no_host": 4194304, "no_ip": 4194302},
    {"cidr": 9, "mask_address": "255.128.0.0", "wild_card": "0.127.255.255", "no_host": 8388608, "no_ip": 8388606},
    {"cidr": 8, "mask_address": "255.0.0.0", "wild_card": "0.255.255.255", "no_host": 16777216, "no_ip": 16777214},
    {"cidr": 7, "mask_address": "254.0.0.0", "wild_card": "1.255.255.255", "no_host": 33554432, "no_ip": 33554430},
    {"cidr": 6, "mask_address": "252.0.0.0", "wild_card": "3.255.255.255", "no_host": 67108864, "no_ip": 67108862},
    {"cidr": 5, "mask_address": "248.0.0.0", "wild_card": "7.255.255.255", "no_host": 134217728, "no_ip": 134217726},
    {"cidr": 4, "mask_address": "240.0.0.0", "wild_card": "15.255.255.255", "no_host": 268435456, "no_ip": 268435454},
    {"cidr": 3, "mask_address": "224.0.0.0", "wild_card": "31.255.255.255", "no_host": 536870912, "no_ip": 536870910},
    {"cidr": 2, "mask_address": "192.0.0.0", "wild_card": "63.255.255.255", "no_host": 1073741824, "no_ip": 1073741822},
    {"cidr": 1, "mask_address": "128.0.0.0", "wild_card": "127.255.255.255", "no_host": 2147483648, "no_ip": 2147483646},
    {"cidr": 0, "mask_address": "0.0.0.0", "wild_card": "255.255.255.255", "no_host": 4294967296, "no_ip": 4294967294},
]


def as_dict(row: BaseModel) -> dict[str, Any]:  # type: ignore
    # table: dict[Any, Any] = getattr(row, "__table__")
    # logger.info("Table data")
    # logger.info(table)
    dict_values: dict[str, Any] = {}
    for column in row.__table__.columns:  # type: ignore
        value_any: Any = getattr(row, column.name)  # type: ignore

        if isinstance(value_any, enum.Enum):
            value_any = value_any.value

        dict_values[column.name] = value_any  # type: ignore

    return dict_values


def get_table_mask_address(cidr: int = -1, mask_address: str = "") -> list[dict[str, Any]]:
    if cidr != -1 and (cidr >= 0 and cidr <= 32):
        return [next(mask for mask in __mask_address if mask["cidr"] == cidr)]
    elif mask_address:
        return [next(mask for mask in __mask_address if mask["mask_address"] == mask_address)]
    else:
        return __mask_address


def get_ip_range(address: str, cidr: int = -1, mask_address: str = "") -> ipaddress.IPv4Network | None:
    value: str = ""
    if cidr != -1 and (cidr >= 0 and cidr <= 32):
        value = str(cidr)
    elif mask_address:
        value = mask_address
    else:
        return None

    return ipaddress.IPv4Network(f"{address}/{value}", strict=False)


def check_enum_value_exist(value: Any, enum_check: list[Any]) -> bool:
    values: list[Any] = [m for m in enum_check]

    return value in values
