# **DeviceView | Visualiza dispositivos de la red**

---

Visualiza de manera rapida los dispositivos que tienes en red mediante el uso del protocolo SNMP

---

Esta realizado con:

-   Lenguage: **Python**, **JS**, **jQuery**, **HTML**
-   Diseño: **Bootstrap 5**, **css**
-   Template engine: **Jinja**
-   Frameworks: **Flask**, **SQLAlchemy**
-   Librerias: **PySNMP**

---

Características de la versión:

-   Acceso a DB con MySQL empleando **SQLALchemy**
-   Sistema de usuarios, y clasificación por areas (Lugares)
-   Sistema de alta, modificación, listar, eliminar y visualizar de: _usuarios_, _dispositivos_, _comunidades snmp_, _areas_

---

## **Imágenes**

![Versión actual](images/acceder-al-sistema.png)
![Versión actual](images/bienvenida.png)
![Versión actual](images/equipos.png)
![Versión actual](images/comunidades.png)
![Versión actual](images/usuarios.png)
![Versión actual](images/areas.png)
![Versión actual](images/desconectarse-sistema.png)

---

## Para ejecutar se debe:

1. Clonar el repositorio
2. Tener instalado [docker](https://docs.docker.com/engine/install/) y [docker-compose](https://docs.docker.com/compose/install/)
3. Abrir una terminal en el directorio donde se clonó el repositorio
4. Construir la imagen del Dockerfile

```
docker build -t blackblexo/flask_app .
```

_\*Nota: se puede cambiar el nombre de **blackblexo/flask_app** por el que quieran, solo se debe cambiar tambien en el **docker-compose.yml**_

5. Levantamos los contenedores

```
docker-compose --env-file=./www/.env up -d
```

6. Abrimos el navegador y ponemos: http://localhost:8080
7. Nos conectamos con las credenciales: _admin@localhost_ y _123_

---

DeviceView | Visualiza cada dispositivo de la red con SNMP

Visualiza de manera simple cada dispositivo de la red con ayuda de SNMP

Author: @BlackBlex (BlackBlex)

License: *MIT*
