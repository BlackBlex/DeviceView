FROM python:3.10.7-alpine

WORKDIR /usr/src/app
COPY requirements.txt ./
RUN python -m venv .venv
RUN .venv/bin/pip install --no-cache-dir wheel
RUN apk add --no-cache build-base && \
    .venv/bin/pip install --no-cache-dir -r requirements.txt && \
    apk del --no-cache build-base && \
    rm -rf /var/cache/apk/*

RUN mkdir -p /usr/src/app/www

WORKDIR /usr/src/app/www
CMD [ "../.venv/bin/python", "-m", "flask", "run"]
